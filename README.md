# Welcome to the GIT Lab for the CS5024 Course - Year 2020

#### Here you will find the place for the deliverables (project 1 and 2, respectively).
#### You will need to place your code in the assigned folder according to your group. If you have any kind of doubt, you can ask in the Sulis's forums.

#### Happy coding!,

#### The CS5024 team
